import 'package:flutter/material.dart';
import 'package:tekotest/src/models/color.model.dart';
import 'package:tekotest/src/models/error_product.model.dart';
import 'package:tekotest/src/repositories/color.repo.dart';
import 'package:tekotest/src/repositories/product.repo.dart';
import 'package:tekotest/src/utils/validator.dart';

enum Status { NO_INTERNET, FAILED, SUCCESS, LOADING, NONE }

class HomeViewModel extends ChangeNotifier {
  Status _status = Status.NONE;
  Status get status => _status;

  List<ErrorProductModel> _errorProducts = [];
  List<ErrorProductModel> get errorProducts => _errorProducts;

  Map<int, String> _colors = {};
  Map<int, String> get colors => _colors;

  Future<void> fetchData() async {
    try {
      _status = Status.LOADING;
      notifyListeners();

      final colorData = await ColorRepository.getAll();
      for (var item in colorData) {
        _colors[item.id ?? 0] = item.name ?? "";
      }

      _errorProducts = await ErrorProductRepository.getAll();
      for (var item in _errorProducts) {
        item.colorName = _colors[item.color];
      }

      _status = Status.SUCCESS;
    } catch (e) {
      _status = Status.FAILED;
    } finally {
      notifyListeners();
    }
  }

  Future<void> refreshData() async {
    try {
      var colorData = await ColorRepository.getAll();
      var productData = await ErrorProductRepository.getAll();

      for (var item in colorData) {
        _colors[item.id ?? 0] = item.name ?? "";
      }

      for (var item in productData) {
        item.colorName = _colors[item.color];
      }
      _errorProducts = productData;
    } catch (e) {
    } finally {
      notifyListeners();
    }
  }

  Future<void> getColor() async {
    final colorData = await ColorRepository.getAll();
    for (var item in colorData) {
      _colors[item.id ?? 0] = item.name ?? "";
    }
  }

  Future<void> getProduct() async {
    _errorProducts = await ErrorProductRepository.getAll();
    for (var item in _errorProducts) {
      item.colorName = _colors[item.color];
    }
  }

  updateData(ErrorProductModel newData) {
    var index =
        _errorProducts.indexWhere((element) => element.id == newData.id);
    if (index != -1) {
      //Validators.isValidSku(newData);
      newData.colorName = _colors[newData.color];
      newData.isEdited = true;
      _errorProducts[index] = newData;
      notifyListeners();
      Validators.isValidProductName(newData);
      Validators.isValidSku(newData);
    }
  }

  bool validate() {
    bool error = false;
    for (var item in _errorProducts) {
      if (item.nameError != null || item.skuError != null) {
        item.isError = true;
        error = true;
      } else {
        item.isError = false;
      }
    }
    notifyListeners();
    return error;
  }

  submitDate() {
    for (var item in _errorProducts) {
      item.isEdited = false;
    }
    notifyListeners();
  }
}
