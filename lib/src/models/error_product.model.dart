class ErrorProductModel {
  int? id;
  String? errorDescription;
  String? name;
  String? sku;
  String? image;
  int? color;

  ErrorProductModel(
      {this.id,
      this.errorDescription,
      this.name,
      this.sku,
      this.image,
      this.color});

  ErrorProductModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    errorDescription = json['errorDescription'];
    name = json['name'];
    sku = json['sku'];
    image = json['image'];
    color = json['color'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['errorDescription'] = this.errorDescription;
    data['name'] = this.name;
    data['sku'] = this.sku;
    data['image'] = this.image;
    data['color'] = this.color;
    return data;
  }

  String? colorName;
  bool isEdited = false;
  bool isError = false;
  String? nameError;
  String? skuError;
}
