import 'package:dio/dio.dart';

class DioInterceptors extends Interceptor {
  @override
  Future onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    print('REQUEST [${options.method}]=>PATH: ${options.path}');
    // options.headers[String.contentType] = AppStrings.applicationContentType;
    super.onRequest(options, handler);
  }

  @override
  Future onResponse(
      Response response, ResponseInterceptorHandler handler) async {
    print(
        'RESPONSE[${response.statusCode}]=>PATH: ${response.requestOptions.path}');
    super.onResponse(response, handler);
  }

  @override
  Future onError(DioException err, ErrorInterceptorHandler handler) async {
    print(
        'ERROR[${err.response?.statusCode}]=>PATH: ${err.requestOptions.path}');
    super.onError(err, handler);
  }
}
