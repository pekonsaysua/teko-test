import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:tekotest/src/core/dio_exception.dart';
import 'package:tekotest/src/core/dio_factory.dart';
import 'package:tekotest/src/share/data_state.dart';

class ApiService {
  static String endPoint = "https://hiring-test.stag.tekoapis.net/api";

  static BaseOptions options = BaseOptions(
      baseUrl: endPoint,
      responseType: ResponseType.plain,
      connectTimeout: const Duration(seconds: 30),
      receiveTimeout: const Duration(seconds: 30),
      validateStatus: (code) {
        return code == 200 || code == 201;
      });
  static Dio dio = DioFactory(endPoint).create();

  static Future<DataState> get(
      {required String path,
      Map<String, dynamic>? query,
      Map<String, dynamic>? header}) async {
    try {
      Response response = await dio.get(path,
          queryParameters: query, options: Options(headers: header));
      return DataSuccess(json.decode(response.data));
    } on DioException catch (e) {
      final errorMessage = MyDioException.fromDioError(e).toString();
      return DataFailed(errorMessage);
    } catch (e) {
      return DataFailed(e.toString());
    }
  }

  static Future<DataState> post(
      {required String path,
      Map<String, dynamic>? data,
      Map<String, dynamic>? header}) async {
    try {
      Response response = await dio.post(
        path,
        data: data,
        options: Options(headers: header),
      );
      return DataSuccess(json.decode(response.data));
    } on DioException catch (e) {
      final errorMessage = MyDioException.fromDioError(e).toString();
      return DataFailed(errorMessage);
    } catch (e) {
      return DataFailed(e.toString());
    }
  }

  static Future<DataState> postFile(
      {required String path,
      required File file,
      Map<String, dynamic>? header}) async {
    try {
      Response response = await dio.post(
        path,
        data:
            FormData.fromMap({"file": await MultipartFile.fromFile(file.path)}),
        options: Options(
            headers: {"Content-Type": "multipart/form-data", ...header ?? {}}),
      );
      return DataSuccess(json.decode(response.data));
    } on DioException catch (e) {
      final errorMessage = MyDioException.fromDioError(e).toString();
      return DataFailed(errorMessage);
    } catch (e) {
      return DataFailed(e.toString());
    }
  }
}
