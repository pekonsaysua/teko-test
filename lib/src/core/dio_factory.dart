import 'package:dio/dio.dart';
import 'package:tekotest/src/core/dio_interceptor.dart';

class DioFactory {
  final String _baseUrl;

  DioFactory(this._baseUrl);

  BaseOptions _createBaseOptions() => BaseOptions(
      baseUrl: _baseUrl,
      responseType: ResponseType.plain,
      receiveTimeout: const Duration(seconds: 30),
      sendTimeout: const Duration(seconds: 30),
      connectTimeout: const Duration(seconds: 30),
      validateStatus: (code) {
        return code == 200 || code == 201;
      });

  Dio create() => Dio(_createBaseOptions())
    ..interceptors.addAll([
      DioInterceptors(),
    ]);
}
