import 'package:flutter/material.dart';
import 'package:tekotest/src/utils/theme.dart';

class CustomLabel extends StatelessWidget {
  final String label;
  final String? value;

  const CustomLabel({Key? key, required this.label, this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "$label: ",
            style: blackTextStyle.copyWith(
              fontWeight: semiBold,
            ),
          ),
          const SizedBox(
            height: 2,
          ),
          Expanded(
            child: Text(
              value ?? "",
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }
}
