import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:tekotest/src/utils/theme.dart';

class CustomKeyValue {
  late String key;
  late String value;

  CustomKeyValue({required this.key, required this.value});

  CustomKeyValue.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    value = json['value'];
  }
}

class CustomDropdownButton extends StatefulWidget {
  final String title;
  final List<CustomKeyValue> items;
  final TextEditingController? controller;
  final String? hintText;
  final TextInputType? keyboardType;
  final Function(String? value)? onChange;
  final String? initialValue;

  const CustomDropdownButton(
      {super.key,
      required this.title,
      required this.items,
      this.controller,
      this.hintText,
      this.keyboardType,
      this.onChange,
      this.initialValue});

  @override
  State<CustomDropdownButton> createState() => _CustomDropdownButtonState();
}

class _CustomDropdownButtonState extends State<CustomDropdownButton> {
  late String? selectedValue;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    selectedValue = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: blackTextStyle.copyWith(
            fontWeight: semiBold,
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        Form(
          key: _formKey,
          child: DropdownButtonFormField2(
            decoration: InputDecoration(
              isDense: true,
              contentPadding: EdgeInsets.zero,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
            isExpanded: true,
            hint: Text(
              widget.hintText ?? "",
              style: TextStyle(fontSize: 14),
            ),
            items: widget.items
                .map((item) => DropdownMenuItem<String>(
                      value: item.key,
                      child: Text(
                        item.value,
                        style: const TextStyle(
                          fontSize: 14,
                        ),
                      ),
                    ))
                .toList(),
            validator: (value) {
              if (value == null) {
                return 'Vui lòng nhập thông tin';
              }
              return null;
            },
            value: selectedValue,
            onChanged: (value) {
              setState(() {
                selectedValue = value.toString();
              });
              //selectedValue!.value = value.toString();
              if (widget.onChange != null) widget.onChange!(value);
            },
            onSaved: (value) {
              selectedValue = value.toString();
            },
            buttonStyleData: const ButtonStyleData(
              height: 50,
              padding: EdgeInsets.only(left: 0, right: 0),
            ),
            iconStyleData: const IconStyleData(
              icon: Icon(
                Icons.arrow_drop_down,
                color: Colors.black45,
              ),
              iconSize: 30,
            ),
            dropdownStyleData: DropdownStyleData(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
