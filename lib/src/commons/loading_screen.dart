import 'package:flutter/material.dart';
import 'package:tekotest/src/utils/theme.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CircularProgressIndicator(),
        SizedBox(
          height: 8,
        ),
        Text(
          "Loading...",
          style: greenTextStyle.copyWith(
            fontSize: 16,
          ),
        )
      ],
    ));
  }
}
