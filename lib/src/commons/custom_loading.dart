import 'package:flutter/material.dart';
import 'package:tekotest/src/share/global_keys.dart';
import 'package:tekotest/src/utils/theme.dart';

class CustomLoading {
  static void show(String content) async {
    return showDialog<void>(
        context: navigatorKey.currentContext!,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Scaffold(
            body: Container(
              margin:
                  EdgeInsets.only(top: MediaQuery.of(context).size.height / 3),
              child: Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                      width: 20,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(blackColor),
                        strokeWidth: 3,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      content,
                      style: TextStyle(color: blackColor),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  static void close() {
    navigatorKey.currentState?.pop();
  }

  static popUntil(String routeName) {
    navigatorKey.currentState?.popUntil((route) {
      return route.settings.name == routeName;
    });
  }
}
