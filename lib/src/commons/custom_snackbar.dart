import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:tekotest/src/share/global_keys.dart';
import 'package:tekotest/src/utils/theme.dart';

class CustomSnackbar {
  static void showCustomSnackBar(String message, {Color? color}) {
    Flushbar(
      message: message,
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: color ?? redColor,
      duration: const Duration(seconds: 2),
    ).show(snackbarKey.currentContext!);
  }
}
