import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tekotest/src/utils/images.dart';

class CustomNetworkImage extends StatelessWidget {
  final String? image;
  const CustomNetworkImage({super.key, required this.image});

  @override
  Widget build(BuildContext context) {
    if (image == null || image!.isEmpty) {
      return Image.asset(imgError);
    } else {
      return CachedNetworkImage(
        imageUrl: image!,
        placeholder: (context, url) =>
            const Center(child: CircularProgressIndicator()),
        errorWidget: (context, url, error) => Image.asset(imgError),
      );
    }
  }
}
