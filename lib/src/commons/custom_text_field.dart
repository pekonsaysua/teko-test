import 'package:flutter/material.dart';
import 'package:tekotest/src/utils/theme.dart';

class CustomTextField extends StatelessWidget {
  final String title;
  final bool obscureText;
  final TextEditingController? controller;
  final String? hintText;
  final TextInputType? keyboardType;
  final int? inputMaxLength; // Add this parameter
  final Function(String? value)? onChange;
  final String? initialValue;
  final bool? disabled;

  const CustomTextField({
    Key? key,
    required this.title,
    this.obscureText = false,
    this.controller,
    this.hintText,
    this.keyboardType,
    this.inputMaxLength, // Add this parameter
    this.onChange,
    this.initialValue,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              title,
              style: blackTextStyle.copyWith(
                fontWeight: semiBold,
              ),
            ),
            // Tooltip(
            //   message: "Tool tip",
            //   child: CircleAvatar(
            //     backgroundColor: greyColor,
            //     child: Icon(
            //       Icons.question_mark_rounded,
            //     ),
            //   ),
            // ),
          ],
        ),
        const SizedBox(
          height: 2,
        ),
        TextFormField(
          initialValue: initialValue,
          readOnly: disabled ?? false,
          obscureText: obscureText,
          controller: controller,
          keyboardType: keyboardType,
          maxLength: inputMaxLength, // Set the maximum length
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Vui lòng nhập ${title}';
            }
            if (inputMaxLength != null && value.length > inputMaxLength!) {
              return 'Vui lòng nhập tối đa $inputMaxLength ký tự';
            }
            return null;
          },
          onChanged: (value) {
            if (onChange != null) onChange!(value);
          },
          decoration: InputDecoration(
            hintText: hintText,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(14),
            ),
            contentPadding: const EdgeInsets.all(12),
          ),
        ),
      ],
    );
  }
}
