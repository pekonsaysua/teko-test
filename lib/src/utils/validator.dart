import 'package:tekotest/src/models/error_product.model.dart';

class Validators {
  // Check email
  static bool isValidProductName(ErrorProductModel product) {
    if (product.name == null || product.name!.isEmpty) {
      product.nameError = "Product Name is required";
      return false;
    }
    if (product.name!.length > 50) {
      product.nameError = "Product Name must be less than 50";
      return false;
    }
    product.nameError = null;
    return true;
  }

  static bool isValidSku(ErrorProductModel product) {
    if (product.sku == null || product.sku!.isEmpty) {
      product.skuError = "SKU is required";
      return false;
    }
    if (product.sku!.length > 20) {
      product.skuError = "SKU must be less than 20";
      return false;
    }
    product.skuError = null;
    return true;
  }
}
