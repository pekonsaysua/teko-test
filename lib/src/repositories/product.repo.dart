import 'package:tekotest/src/core/api.service.dart';
import 'package:tekotest/src/models/error_product.model.dart';
import 'package:tekotest/src/share/data_state.dart';

class ErrorProductRepository {
  static Future<List<ErrorProductModel>> getAll() async {
    final res = await ApiService.get(path: "/products");
    if (res is DataFailed) return [];
    return List<ErrorProductModel>.from(
        res.data.map((x) => ErrorProductModel.fromJson(x)));
  }
}
