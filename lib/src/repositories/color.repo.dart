import 'package:tekotest/src/core/api.service.dart';
import 'package:tekotest/src/models/color.model.dart';
import 'package:tekotest/src/share/data_state.dart';

class ColorRepository {
  static Future<List<ColorModel>> getAll() async {
    final res = await ApiService.get(path: "/colors");
    if (res is DataFailed) return [];
    return List<ColorModel>.from(res.data.map((x) => ColorModel.fromJson(x)));
  }
}
