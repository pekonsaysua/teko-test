import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';
import 'package:tekotest/src/commons/custom_network_image.dart';
import 'package:tekotest/src/models/error_product.model.dart';
import 'package:tekotest/src/utils/theme.dart';
import 'package:tekotest/src/viewmodels/home.viewmodel.dart';

class ErrorProductWidget extends StatelessWidget {
  final ErrorProductModel errorProduct;
  final bool? isEdit;
  final VoidCallback? onPressed;

  const ErrorProductWidget({
    super.key,
    required this.errorProduct,
    this.isEdit = false,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<HomeViewModel>(context);
    return Card(
      color: whiteColor,
      surfaceTintColor: whiteColor,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.zero,
      ),
      borderOnForeground: false,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 130,
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: CustomNetworkImage(image: errorProduct.image),
                  ),
                ),
                Expanded(
                    flex: 5,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        _buildText(
                            text: errorProduct.name,
                            errorText: "noname",
                            maxline: 2),
                        _buildText(
                            text: errorProduct.errorDescription,
                            errorText: "nodescripttion"),
                        _buildText(text: errorProduct.sku, errorText: "nosku"),
                        _buildText(
                            text: errorProduct.colorName, errorText: "nocolor"),
                      ],
                    )),
                isEdit!
                    ? Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: GestureDetector(
                          onTap: onPressed,
                          child: Icon(Icons.edit, color: redColor),
                        ),
                      )
                    : const SizedBox(),
              ],
            ),
          ),
          isEdit! && errorProduct.isError
              ? Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      errorProduct.nameError == null
                          ? const SizedBox()
                          : Text(
                              errorProduct.nameError ?? "",
                              style: redTextStyle,
                            ),
                      errorProduct.skuError == null
                          ? const SizedBox()
                          : Text(errorProduct.skuError ?? "",
                              style: redTextStyle)
                    ],
                  ),
                )
              : const SizedBox()
        ],
      ),
    );
  }

  Widget _buildText(
      {required String? text, int? maxline = 1, String errorText = ""}) {
    bool isValidText = text != null && text.isNotEmpty;
    return Text(
      isValidText ? text : errorText,
      maxLines: maxline,
      overflow: TextOverflow.ellipsis,
      style: isValidText ? TextStyle() : greyTextStyle,
    );
  }
}
