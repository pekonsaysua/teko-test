import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tekotest/src/commons/custom_dropdown_button.dart';
import 'package:tekotest/src/commons/custom_label.dart';
import 'package:tekotest/src/commons/custom_network_image.dart';
import 'package:tekotest/src/commons/custom_text_field.dart';
import 'package:tekotest/src/models/error_product.model.dart';
import 'package:tekotest/src/viewmodels/home.viewmodel.dart';

class ProductUpdateWidget extends StatelessWidget {
  final ErrorProductModel errorProduct;

  const ProductUpdateWidget({super.key, required this.errorProduct});

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<HomeViewModel>(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
            height: 100,
            child:
                Center(child: CustomNetworkImage(image: errorProduct.image))),
        CustomLabel(
          label: "ID",
          value: errorProduct.id.toString(),
        ),
        CustomLabel(
          label: "Error Description",
          value: errorProduct.errorDescription,
        ),
        const SizedBox(
          height: 8,
        ),
        CustomTextField(
          title: "Product Name",
          initialValue: errorProduct.name,
          onChange: (value) {
            errorProduct.name = value;
          },
        ),
        const SizedBox(
          height: 8,
        ),
        CustomTextField(
          title: "SKU",
          initialValue: errorProduct.sku,
          onChange: (value) {
            errorProduct.sku = value;
          },
        ),
        const SizedBox(
          height: 8,
        ),
        CustomDropdownButton(
          title: "Color",
          hintText: "Choose color",
          initialValue: errorProduct.color?.toString(),
          items: vm.colors.entries
              .map((e) => CustomKeyValue(key: e.key.toString(), value: e.value))
              .toList(),
          onChange: (value) {
            errorProduct.color = int.parse(value ?? "0");
          },
        ),
      ],
    );
  }
}
