import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tekotest/src/commons/custom_button.dart';
import 'package:tekotest/src/commons/custom_snackbar.dart';
import 'package:tekotest/src/commons/loading_screen.dart';
import 'package:tekotest/src/models/error_product.model.dart';
import 'package:tekotest/src/share/snackbar.dart';
import 'package:tekotest/src/utils/images.dart';
import 'package:tekotest/src/utils/theme.dart';
import 'package:tekotest/src/viewmodels/home.viewmodel.dart';
import 'package:tekotest/src/views/home/components/product.widget.dart';
import 'package:tekotest/src/views/home/components/product_update.widget.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final itemPerPage = 10;

  @override
  void initState() {
    // TODO: implement initState
    Future.delayed(
      Duration.zero,
      () {
        Provider.of<HomeViewModel>(context, listen: false).fetchData();
      },
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<HomeViewModel>(context);
    int pageCount = (vm.errorProducts.length / itemPerPage).ceil();

    return Scaffold(
      body: vm.status == Status.LOADING
          ? const LoadingScreen()
          : Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Expanded(
                      child: vm.errorProducts.isEmpty
                          ? _buildNodata(context, vm)
                          : _buildPageView(context, vm, pageCount)),
                  CustomFilledButton(
                    title: "SUBMIT",
                    onPressed: () {
                      _handleSubmit(context, vm);
                    },
                  ),
                ],
              ),
            ),
    );
  }

  Widget _buildNodata(BuildContext context, HomeViewModel vm) {
    return RefreshIndicator(
      onRefresh: vm.refreshData,
      child: ListView(
        children: [
          SizedBox(
            height: 0.25 * MediaQuery.of(context).size.height,
          ),
          Image.asset(
            imgNodata,
            height: 100,
          ),
          const SizedBox(
            height: 30,
          ),
          Center(child: Text("No data")),
        ],
      ),
    );
  }

  Widget _buildPageView(BuildContext context, HomeViewModel vm, int pageCount) {
    return PageView.builder(
      itemCount: pageCount,
      itemBuilder: (context, index) {
        int startIndex = index * itemPerPage;
        int endIndex = (index + 1) * itemPerPage;
        endIndex = endIndex > vm.errorProducts.length
            ? vm.errorProducts.length
            : endIndex;

        List<ErrorProductModel> currentPageData =
            vm.errorProducts.sublist(startIndex, endIndex);
        return RefreshIndicator(
            onRefresh: vm.refreshData,
            child: _buildList(context, currentPageData, true));
      },
    );
  }

  Widget _buildList(BuildContext context, List errorProducts, bool isEdit) {
    return ListView.separated(
      itemCount: errorProducts.length,
      itemBuilder: (context, index) {
        var product = errorProducts[index];
        return ErrorProductWidget(
          errorProduct: product,
          isEdit: isEdit,
          onPressed: () {
            _handleEdit(context, product);
          },
        );
      },
      separatorBuilder: (context, index) {
        return const SizedBox(
          height: 5,
        );
      },
    );
  }

  void _handleEdit(BuildContext context, ErrorProductModel errorProduct) {
    showDialog(
      context: context,
      builder: (context) {
        final vm = Provider.of<HomeViewModel>(context);
        return AlertDialog(
          insetPadding: const EdgeInsets.symmetric(horizontal: 20),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
          actionsPadding: EdgeInsets.zero,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10))),
          title: const Text("Edit error product"),
          content: SizedBox(
            height: 350,
            width: 500,
            child: SingleChildScrollView(
              child: ProductUpdateWidget(
                errorProduct: errorProduct,
              ),
            ),
          ),
          actions: [
            TextButton(
                onPressed: () {
                  vm.updateData(errorProduct);
                  Navigator.pop(context);
                },
                child: Text("OK"))
          ],
        );
      },
    );
  }

  void _handleSubmit(BuildContext context, HomeViewModel provider) {
    final isEditError = provider.validate();
    final editErrorProducts =
        provider.errorProducts.where((element) => element.isEdited).toList();
    if (isEditError) {
      showCustomSnackBar(
        context,
        "Have some error. Please fix it",
      );
    } else if (editErrorProducts.isEmpty) {
      showCustomSnackBar(
        context,
        "Don't have any update",
      );
    } else {
      showDialog(
        context: context,
        builder: (context) {
          final vm = Provider.of<HomeViewModel>(context);
          final editErrorProducts =
              vm.errorProducts.where((element) => element.isEdited).toList();
          return AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            title: const Text("Submit Changes"),
            content: SizedBox(
              height: 355,
              width: 500,
              child: _buildList(context, editErrorProducts, false),
            ),
            insetPadding: const EdgeInsets.symmetric(horizontal: 20),
            contentPadding: const EdgeInsets.all(5),
            actionsPadding: EdgeInsets.zero,
            actions: [
              TextButton(
                  onPressed: () {
                    vm.submitDate();
                    Navigator.of(context).pop(editErrorProducts.length > 0);
                  },
                  child: Text("OK"))
            ],
          );
        },
      ).then((value) => {
            if (value != null)
              {
                if (value)
                  {
                    showCustomSnackBar(context, "Submit successfully",
                        color: greenColor)
                  }
              }
          });
    }
  }
}
