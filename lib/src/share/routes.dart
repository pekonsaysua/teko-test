import 'package:tekotest/src/views/home/home.view.dart';

const initialRoute = "/";

var routes = {
  '/': (context) => const HomeView(),
};
